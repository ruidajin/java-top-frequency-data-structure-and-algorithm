package design_pattern.simplefactory;

public class SimpleFactory {
    public Product createProduct(int type){
        if(type == 1){
            return new ConcreteProduct1();
        }else{
            return new ConcreteProduct2();
        }
    }
}
