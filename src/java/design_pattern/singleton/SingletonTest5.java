package design_pattern.singleton;

/**
 * 懒加载：synchronized效率低，双重检测方式减小锁粒度
 */
public class SingletonTest5 {

}

class Singleton5{
    private static volatile Singleton5 instance;

    private Singleton5(){}

    public static Singleton5 getInstance(){
        if(instance == null){
            synchronized (Singleton5.class){
                if(instance == null){ // volatile及时可见，防止指令重排返回没有初始化完成的对象
                    instance = new Singleton5();
                }
            }
        }
        return instance;
    }
}
