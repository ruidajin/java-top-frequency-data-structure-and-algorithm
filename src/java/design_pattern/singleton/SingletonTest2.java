package design_pattern.singleton;

/**
 * 饿汉式，在静态代码块中创建，只保留了一份，静态的代码块，变量都是属于类的，不同的对象共享这部分数据
 */
public class SingletonTest2 {
    public static void main(String[] args) {
        Singleton2 instance1 = Singleton2.getInstance();
        Singleton2 instance2 = Singleton2.getInstance();
        System.out.println(instance1 == instance2);
        System.out.println("instance.hashCode = "+instance1.hashCode());
        System.out.println("instance.hashCode = "+instance2.hashCode());
    }
}

class Singleton2{
    // 私有构造器，外部不能new
    private Singleton2(){}
    // 私有静态变量，外部不能访问
    private static Singleton2 instance;
    // 静态代码块中创建,类创建的同时创建，保留一份
    static {
        instance = new Singleton2();
    }
    // 提供一个共有静态方法，返回实例对象
    public static Singleton2 getInstance(){
        return instance;
    }
}
