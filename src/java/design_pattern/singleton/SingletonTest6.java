package design_pattern.singleton;

/**
 * 懒汉式，静态内部类完成单例模式，静态内部类是等到用的时候才加载
 * 静态内部类方式在 Singleton 类被装载时并不会立即实例化， 而是在需要实例化时， 调用 getInstance 方法， 才
 * 会装载 SingletonInstance 类， 从而完成 Singleton 的实例化。
 *
 * 类的静态属性只会在第一次加载类的时候初始化， 所以在这里， JVM 帮助我们保证了线程的安全性， 在类进行
 * 初始化时， 别的线程是无法进入的
 */
public class SingletonTest6 {
    public static void main(String[] args) {
        Singleton6 instance1 = Singleton6.getInstance();
        Singleton6 instance2 = Singleton6.getInstance();
        System.out.println(instance1 == instance2);
    }
}

class Singleton6{
    private static Singleton6 instance;

    private Singleton6(){}

    private static class SingletonInstance{
        private static final Singleton6 INSTANCE = new Singleton6();
    }

    public static synchronized Singleton6 getInstance(){
        return SingletonInstance.INSTANCE;
    }
}

