package design_pattern.singleton;

public class SingletonTest7 {
    public static void main(String[] args) {
        Singleton7 instance1 = Singleton7.INSTANCE;
        Singleton7 instance2 = Singleton7.INSTANCE;
        System.out.println(instance1==instance2);
    }
}

enum Singleton7{
    INSTANCE;
}
