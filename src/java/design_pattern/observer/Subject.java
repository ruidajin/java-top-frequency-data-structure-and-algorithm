package design_pattern.observer;

/**
 * 提供一个第三方，注册，移除，通知服务
 */
public interface Subject {
    void registerObserver(Observer o);
    void removeObserver(Observer o);
    void notifyObserver();
}
