package design_pattern.responsibilitychain;

public class ConcreateHandler1 extends Handler{
    public ConcreateHandler1(Handler successor) {
        super(successor);
    }

    @Override
    protected void handleRequest(Request request) {
        if(request.getType() == RequestType.type1){
            System.out.println(request.getName()+" is handler by concreateHandler1");
            return;
        }
        if(successor!=null){
            successor.handleRequest(request);
        }
    }
}
