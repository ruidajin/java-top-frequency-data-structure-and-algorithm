package alibabainterview.mathfanbonaci;

import java.util.Scanner;

public class Main{
    static int sNum = 1000000007;
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        for(int i=0; i<T; i++){
            // A, B, n
            int A = sc.nextInt();
            int B = sc.nextInt();
            int n = sc.nextInt();
            int result = getAnswer(A, B, n);
            System.out.println(result);
        }
    }
    public static int getAnswer(int A, int B, int n){
        if(n == 0) return 2;
        if(n == 1) return A;
        long l1 = 2L, l2 = (long)A, temp = 0L;
        for(int i=2; i<=n; i++){
            temp = ((A*l2)%sNum - (B*l1)%sNum + sNum) % sNum;
            l1 = l2;
            l2 = temp;
        }
        return (int)temp;
    }
}
