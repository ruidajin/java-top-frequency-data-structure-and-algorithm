package alibabainterview.numoftriangle;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(getAns(9));
    }
    public static int getAns(int n){
        int min = n/4;
        int max = (n+1)/2;
        int sum = 0;
        for (int i = min+1; i < max; i++) {
            if(n == 3*i){
                sum = sum + n/3;
            }else{
                sum = sum+n;
            }
        }
        return sum;
    }
}
