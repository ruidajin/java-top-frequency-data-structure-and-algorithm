package alibabainterview.partselection;

import java.util.*;

/*
    每个零件有 时间不稳定，空间不稳定
    两两选择，时间和空间相组，和最小。只要是没有约束限制的都可以两两组合
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(); // 零件数
        int m = sc.nextInt(); // 约束数
        int[][] partarr = new int[n][2];
        for (int i = 0; i < n; i++) {
            partarr[i][0] = sc.nextInt();
            partarr[i][1] = sc.nextInt();
        }
        // 每个零件与之相冲突的零件的列表
        Map<Integer, List<Integer>> conflict = new HashMap<>();
        for (int i = 0; i < m; i++) {
            int part1 = sc.nextInt()-1; // 第1变成索引0
            int part2 = sc.nextInt()-1;
            if(conflict.containsKey(part1)){
                conflict.get(part1).add(part2);
            }else{
                conflict.put(part1, new ArrayList<>());
                conflict.get(part1).add(part2);
            }
            if(conflict.containsKey(part2)){
                conflict.get(part2).add(part1);
            }else{
                conflict.put(part2, new ArrayList<>());
                conflict.get(part2).add(part1);
            }
        }
        // 根据零件列表和冲突列表，得到每个零件所产生的的最小不稳定性之和
        getMinStableSum(partarr, conflict);
    }

    public static void getMinStableSum(int[][] partarr, Map<Integer, List<Integer>> conflict){
        int n = partarr.length;
        int[] res = new int[n];
        for (int i = 0; i < n; i++) {
            int sum = 0;
            for (int j = 0; j < n; j++) {
                if(j != i && !conflict.get(i).contains(j)){
                    // 不是原数，不存在于冲突列表，两数之最小不稳定
                    sum += minStableOfTwo(i,j,partarr);
                }
            }
            res[i] = sum;
        }
        for (int i = 0; i < n; i++) {
            System.out.print(res[i]+" ");
        }
    }
    public static int minStableOfTwo(int i, int j, int[][] partarr){
        return Math.min(partarr[i][0]+partarr[j][1], partarr[i][1]+partarr[j][0]);
    }
}

