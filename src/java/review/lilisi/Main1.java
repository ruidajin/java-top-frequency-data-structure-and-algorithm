package review.lilisi;

import java.util.Scanner;

public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n=sc.nextInt();
        int[] a = new int[n];
        int[] f = new int[n];
        int res=0;
        for (int i = 0; i <n; i++) {
            a[i]=sc.nextInt();
        }

        for (int i = 0; i <n; i++) {
            f[i]=a[i];
            for (int j = 0; j < i; j++) {
                //对于每一个小于a[i]的a[j] (j < i)
                //f[i] = max(f[i], f[j] + a[i])
                if(a[j]<a[i]) f[i]=Math.max(f[i], f[j]+a[i]);
            }
            res=Math.max(res, f[i]);
        }
        System.out.println(res);
    }
}
