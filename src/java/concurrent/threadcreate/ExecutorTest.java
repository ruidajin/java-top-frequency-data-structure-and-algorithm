package concurrent.threadcreate;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorTest {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(new RunnableDemo());
    }
}

class RunnableDemo implements Runnable{

    @Override
    public void run() {
        System.out.println("dabin");
    }
}
