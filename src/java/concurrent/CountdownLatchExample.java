package concurrent;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * countdownLatch计数器使用的例子
 */
public class CountdownLatchExample {
    public static void main(String[] args) throws InterruptedException {
        final int totalThread = 10; // 线程的数量是10个，放到线程池中执行
        CountDownLatch countDownLatch = new CountDownLatch(totalThread);
        ExecutorService executorService = Executors.newCachedThreadPool(); //线程池不允许使用Executors去创建，而是通过ThreadPoolExecutor的方式
        for (int i = 0; i < totalThread; i++) {
            executorService.execute(()->{
                System.out.println("run.."+Thread.currentThread().getName());
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        System.out.println("end");
        executorService.shutdown();
    }
}
